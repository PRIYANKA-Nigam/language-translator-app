<?php
$top_nav=array(
    'en'=>array('Home','About Us','Logout','Contents will be shown through change in file via url'),
    'hi'=>array('होम', 'हमारे बारे में', 'लॉगआउट','सामग्री url के माध्यम से फ़ाइल में परिवर्तन के माध्यम से दिखाई जाएगी')
);
$website_content=array(
    'en'=>array('My name is Priyanka Nigam.I am coding in PHP .I work as a developer with backend,frontend,and middleware.',
'Let me tell you a story.','One morning at the breakfast table eight year old Fern sees her father leave the house 
with an axe and asks her mother where he is going. 
Her mother delivers the shocking news that Mr Arable is going out to kill a runt that was born the night before. Fern chases her father 
down and persuades him to spare the runt telling him that it is unjust to kill a piglet just because it is small. Moved by his daughter
plea Mr Arable decides to give the runt to her to look after.','Fern names the piglet Wilbur and looks after him like a baby pushing 
him in her pram alongside her doll and feeding him with a bottle. 
At five weeks old Mr Arable insists that Wilbur is sold and he goes to live in the Zuckerman barn down the road.

Wilbur initially struggles at the barn because he misses Fern so much but soon he becomes acquainted with new friends the best of whom is 
a lady grey spider called Charlotte. Wilbur is fascinated by Charlotte although to begin with he is slightly suspicious of the way she catches 
her food - he does not like the idea that she spins bugs in her web and sucks their blood. He soon realizes that Charlotte is everything but 
cruel and bloodthirsty and that her method of eating is entirely necessary for a spider.'),
    'hi'=>array('मेरा नाम प्रियंका निगम है। मैं PHP में कोडिंग कर रही हूं। मैं बैकएंड, फ्रंटएंड और मिडलवेयर के साथ एक डेवलपर के रूप में काम करती हूं।',
    'मैं तुम्हें एक कहानी सुनाता हूँ।', 'एक सुबह नाश्ते की टेबल पर आठ साल की फ़र्न अपने पिता को घर से निकलते हुए देखती है
    एक कुल्हाड़ी के साथ और अपनी माँ से पूछता है कि वह कहाँ जा रहा है।
    उसकी माँ चौंकाने वाली खबर देती है कि मिस्टर अरबल रात को पैदा हुए एक बच्चे को मारने के लिए बाहर जा रहे हैं। फर्न अपने पिता का पीछा करती है
    नीचे उतरा और उसे यह कहते हुए दौड़ने से रोकने के लिए राजी किया कि एक सुअर को सिर्फ इसलिए मारना अन्याय है क्योंकि यह छोटा है। उनकी बेटी द्वारा ले जाया गया
    दलील मिस्टर एरेबल ने उसकी देखभाल करने के लिए दौड़ लगाने का फैसला किया।', 'फर्न ने पिगलेट विल्बर का नाम लिया और उसकी देखभाल एक बच्चे की तरह की
    उसे अपने प्रैम में अपनी गुड़िया के साथ रखा और उसे एक बोतल से दूध पिलाया।
    पांच सप्ताह की उम्र में मिस्टर एरेबल ने जोर देकर कहा कि विल्बर को बेच दिया गया है और वह सड़क के नीचे ज़करमैन खलिहान में रहने के लिए चला जाता है।
    
    विल्बर शुरू में खलिहान में संघर्ष करता है क्योंकि उसे फ़र्न की बहुत याद आती है लेकिन जल्द ही वह नए दोस्तों से परिचित हो जाता है जिनमें से सबसे अच्छा है
    चार्लोट नामक एक महिला ग्रे मकड़ी। विल्बर शार्लेट पर मोहित है, हालांकि शुरुआत में वह उसके पकड़ने के तरीके को लेकर थोड़ा संदिग्ध है
    उसका भोजन - उसे यह विचार पसंद नहीं है कि वह अपने जाले में कीड़ों को घुमाती है और उनका खून चूसती है। वह जल्द ही महसूस करता है कि शार्लेट सब कुछ है लेकिन
    क्रूर और रक्तपिपासु और यह कि उसके खाने का तरीका मकड़ी के लिए पूरी तरह आवश्यक है।')
);

?>